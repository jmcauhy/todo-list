# To-do List

### Description

This project was developed using HTML, CSS, JavaScript, React, Node.js and MongoDB.
<br/>
The app's current functionalities includes writing, checking, searching and deleting to-dos while every change is simultaneously saved to the database.

![](/frontend/public/demo.gif)
  &larr; (demo in an iPhone X)

### Installation

To run the project on your PC you must have Node.js and MongoDB Compass installed, as well as a MongoDB account.

* Download the LTS version of [Node.js](https://nodejs.org/pt-br/download/);

* Create an account at [MongoDB](https://www.mongodb.com/);

* Login and click on "Clusters", "Build a new cluster" and go on untill it's built;

* Create a new user with rights to the cluster at "Database Access";

* Go to "Network Access" and give your current IP address access to the database;

* Go back to your cluster, hit "Connect", "Connect using MongoDB Compass" and then "Download Compass";

* Copy the link beneath "Download Compass" and open MongoDB Compass;

* Paste the link into MongoDB Compass input box and change the ```<password>``` tag inside it to your actual password;

* Go to the project files, open ```/backend/index.js``` and paste the link to substitute the only commentary in the file;

* Open a terminal inside the project's folder and run ```npm install```;

* To run the backend, open a terminal on ```/backend``` and run ```npm run dev```;

* To run the frontend, open a terminal on ```/frontend``` and run ```npm run start```.

### License

[MIT](https://choosealicense.com/licenses/mit/)