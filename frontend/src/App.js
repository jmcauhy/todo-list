import React, { useState } from 'react'

import Header from './components/Header'
import Main from './components/Main'

import './App.css'
import './media.css'

function App() {
	const [todoList, setTodoList] = useState([]),
			[input, setInput] = useState('')

	return (
		<div id="content">
			<Header todoList={todoList} setTodoList={setTodoList} input={input} setInput={setInput} />
			<Main todoList={todoList} setTodoList={setTodoList} setInput={setInput} />
		</div>
	)
}

export default App