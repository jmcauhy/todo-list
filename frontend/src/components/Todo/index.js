import React, { useState, useEffect } from 'react'

import axios from '../../services/axios'

import { InlineIcon } from '@iconify/react'
import ellipseOutline from '@iconify/icons-ion/ellipse-outline'
import checkmarkCircle from '@iconify/icons-ion/checkmark-circle'
import removeCircleOutline from '@iconify/icons-ion/remove-circle-outline'

import './styles.css'

function Todo({ _id, text, isChecked, setTodoList, setInput }) {
   const [currCheck, setCurrCheck] = useState(isChecked),
			[icon, setIcon] = useState(ellipseOutline)

	// This is my solution to a bug. There'll be further update on this
	useEffect(() => {
		function loadCheck() { handleCheck() } loadCheck()
	}, [])

	// Changes the current state of an item and update it to the database
	async function handleCheck() {
		if (currCheck) {
			setIcon(checkmarkCircle)
			setCurrCheck(state => !state)
			return await axios.put('/', { _id, isChecked: currCheck })
		}

		setIcon(ellipseOutline)
		setCurrCheck(state => !state)
		return await axios.put('/', { _id, isChecked: currCheck })
	}

	// Listens to the click on the delete button
	async function handleDelete() {
		// Deletes in the database the clicked item
		await axios.delete(`/${_id}`)

		// Brings the current list in the database after deleting the item
		const response = await axios.get('/')

		// Substitutes the current list on the app with the current list in the database
		setTodoList(response.data)

		// Empties the input field in case it was being used
		setInput('')
	}

	return (
		<li className="todo" id={_id}>
			<span className="check" onClick={handleCheck}><InlineIcon icon={icon} /></span>

			<span className="text">{text}</span>

			<span className="delete" onClick={handleDelete}><InlineIcon icon={removeCircleOutline} /></span>
		</li>
	)
}

export default Todo