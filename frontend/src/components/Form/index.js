import React from 'react'

import axios from '../../services/axios'

import './styles.css'

function Form({ todoList, setTodoList, text, setText }) {
   async function handleSubmit(e) {
      e.preventDefault()

      if (text.length) {
         // Adds the new to-do to the database
         const response = await axios.post('/', { text, isChecked: false })

         // Adds the new to-do to the current list
         setTodoList([...todoList, response.data])

         // Cleans the input box after sending
         return setText('')
      }

      return alert('You need to type something!')
   }

   return (
      <form onSubmit={handleSubmit}>
         <input
            placeholder="Type in a new to-do..."
            type="text"
            value={text}
            onChange={e => setText(e.target.value)}
         />
      </form>
   )
}

export default Form