import React, { useEffect } from 'react'

import axios from '../../services/axios'

import './styles.css'

function Search({ todoList, setTodoList, input, setInput }) {
   // Returns the todoList in the database back to the app if the search box is empty
   useEffect(() => {
		async function loadTodos() {
         if (!input.length) {
				const response = await axios.get('/')

				setTodoList(response.data)
			}
      }

		loadTodos()
   }, [input])

   // Creates a new array that matches the input in the search box and make it the new todoList
   useEffect(() => {
      async function handleSearch() {
         const inputMatches = []

         todoList.map(todo => {
            if (todo.text.includes(input)) inputMatches.push(todo)
         })

         return setTodoList(inputMatches)
      }

      handleSearch()
   }, [input])

   return (
      <input
         type="search"
         value={input}
         onChange={e => setInput(e.target.value)}
         placeholder="Search for a to-do on the list..."
      />
   )
}

export default Search