import React, { useState } from 'react'

import Todo from '../Todo'
import Form from '../Form'

import './styles.css'

function Main({ todoList, setTodoList, setInput }) {
   const [text, setText] = useState('')

   return (
      <main>
         {todoList.map(todo =>
            <Todo
               key={todo._id}
               _id={todo._id}
               text={todo.text}
               isChecked={todo.isChecked}
               setTodoList={setTodoList}
               setInput={setInput}
            />
         )}

         <Form
            todoList={todoList}
            setTodoList={setTodoList}
            text={text}
            setText={setText}
         />
      </main>
   )
}

export default Main