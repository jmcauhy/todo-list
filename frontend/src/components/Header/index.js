import React from 'react'

import Search from '../Search'

import './styles.css'

function Header({ todoList, setTodoList, input, setInput }) {
   return (
      <header>
         <h1>To-do List</h1>

         <Search
            todoList={todoList}
            setTodoList={setTodoList}
            input={input}
            setInput={setInput}
         />
      </header>
   )
}

export default Header