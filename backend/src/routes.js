const { Router } = require('express'),
      routes = Router()

const TodoController = require('./controllers/TodoController')

routes.get('/', TodoController.index)
routes.post('/', TodoController.store)
routes.put('/', TodoController.update)
routes.delete('/:id', TodoController.delete)

module.exports = routes