const mongoose = require('mongoose')

const TodoSchema = new mongoose.Schema({ text: String, isChecked: Boolean })

module.exports = mongoose.model('Todo', TodoSchema)