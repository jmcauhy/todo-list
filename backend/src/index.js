const express = require('express'),
      mongoose = require('mongoose'),
      cors = require('cors'),
      routes = require('./routes')

const app = express()

mongoose.connect(/* 'PASTE THE MONGODB COMPASS LINK HERE' */, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})

app.use(cors({ origin: 'http://localhost:3000' }))

app.use(express.json())

app.use(routes)

app.listen(3333)