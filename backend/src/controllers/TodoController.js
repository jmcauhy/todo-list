const Todo = require('../models/Todo')

module.exports = {
   async index(req, res) {
      const todoList = await Todo.find()

      return res.json(todoList)
   },

   async store(req, res) {
      const { text, isChecked } = req.body

      const todo = await Todo.create({ text, isChecked })

      return res.json(todo)
   },

   async update(req, res) {
      const { _id, isChecked } = req.body

      const todo = await Todo.updateOne({ _id }, { isChecked })

      return res.json(todo)
   },

   async delete(req, res) {
      const { id } = req.params

      const todo = await Todo.deleteOne({ _id: id })

      return res.json(todo)
   }
}